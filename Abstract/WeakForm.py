'''
=======================================
Defines abstract weak forms.

Author:             Johann Rudi
=======================================
'''

import fenics as fe
import sys

#=======================================
# Objective
#=======================================

def _huber_loss_fn(U, alpha, delta):
    '''
    Creates the Huber loss function (without integral):

                   { alpha/2*|U|^2,      if alpha*|U| <= delta
        Huber(U) = {
                   { delta*|U| - delta^2/(2*alpha),  otherwise

                   { alpha/2*|U|^2,                  if |U| <= delta/alpha
                 = {
                   { delta/2*|U| + delta/2*(|U| - delta/alpha),  otherwise
    where

        0 < alpha, delta
    '''
    magn_sq = fe.inner(U, U)
    magn    = fe.sqrt(fe.inner(U, U))
    return fe.conditional( fe.lt(alpha*magn, delta), 0.5*alpha*magn_sq,
                           0.5*delta*magn + 0.5*delta/alpha*(alpha*magn - delta) )

def objective(U, alpha, delta, reg=None, shift=None, dx=None):
    '''
    Creates the weak form for the objetive functional:

        \int Huber(U) + reg/2*|U|^2 + shift
    '''
    obj = _huber_loss_fn(U, alpha, delta)

    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        obj = obj + 0.5*reg*fe.inner(U, U)

    if shift is not None:
        if isinstance(shift, float):
            obj = obj + fe.Constant(shift)
        else:
            obj = obj + shift

    if dx is not None:
        return (obj)*dx

    return (obj)*fe.dx

#=======================================
# Linearization
#=======================================

def gradient(U, U_test, alpha, delta, reg=None, dx=None):
    '''
    Creates the weak form for the gradient:

             { alpha,  if alpha*|U| <= delta }
        \int {                               }*(U,U_test)
             { delta/|U|,          otherwise }
    '''
    magn = fe.sqrt(fe.inner(U, U))
    grad = fe.conditional(fe.lt(alpha*magn, delta), alpha, delta/magn) * fe.inner(U, U_test)

    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        grad = grad + reg*fe.inner(U, U_test)
    if dx is not None:
        return (grad)*dx
    return (grad)*fe.dx

def hessian_Picard(U, U_trial, U_test, alpha, delta, reg=None, dx=None):
    '''
    Creates the weak form for the Hessian of the Picard linearization:

             { alpha,  if alpha*|U| <= delta }
        \int {                               }*(U_trial,U_test)
             { delta/|U|,          otherwise }
    '''
    magn = fe.sqrt(fe.inner(U, U))
    hess = fe.conditional(fe.lt(alpha*magn, delta), alpha, delta/magn) * \
           fe.inner(U_trial, U_test)
    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        hess = hess + reg*fe.inner(U_trial, U_test)
    if dx is not None:
        return (hess)*dx
    return (hess)*fe.dx


def hessian_NewtonStandard(U, U_trial, U_test, alpha, delta, reg=None, dx=None):
    '''
    Creates the weak form for the Hessian of the standard Newton linearization:

             { alpha,            if alpha*|U| <= delta }
        \int {                                         }*(U_trial,U_test)
             { delta/|U|*(I - U(x)U/|U|^2),  otherwise }
    '''
    magn_sq = fe.inner(U, U)
    magn = fe.sqrt(fe.inner(U, U))
    hess = fe.conditional(
            fe.lt(alpha*magn, delta), alpha*fe.inner(U_trial, U_test),
            delta/magn * ( fe.inner(U_trial, U_test) - \
                           fe.inner(U_trial, U)*fe.inner(U, U_test)/magn_sq )
    )
    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        hess = hess + reg*fe.inner(U_trial, U_test)
    if dx is not None:
        return (hess)*dx
    return (hess)*fe.dx

def hessian_NewtonPerturbed(U, U_trial, U_test, S, alpha, delta, reg=None, dx=None):
    '''
    Creates the weak form for the Hessian of the standard Newton linearization:

             { alpha,          if |U| <= delta/alpha }
        \int {                                       }*(U_trial,U_test)
             { delta/|U|*(I - U(x)S/|U|),  otherwise }
    '''
    magn = fe.sqrt(fe.inner(U, U))
    hess = fe.conditional(
            fe.lt(alpha*magn, delta), alpha*fe.inner(U_trial, U_test),
            delta/magn * ( fe.inner(U_trial, U_test) - \
                           fe.inner(U_trial, S)*fe.inner(U, U_test)/magn )
    )
    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        hess = hess + reg*fe.inner(U_trial, U_test)
    if dx is not None:
        return (hess)*dx
    return (hess)*fe.dx

def hessian_NewtonPerturbedSym(U, U_trial, U_test, S, alpha, delta, reg=None, dx=None):
    '''
    Creates the weak form for the Hessian of the standard Newton linearization:

             { alpha,                        if |U| <= delta/alpha }
        \int {                                                     }*(U_trial,U_test)
             { delta/|U|*(I - (U(x)S + S(x)U)/(2*|U|)),  otherwise }
    '''
    magn = fe.sqrt(fe.inner(U, U))
    hess = fe.conditional(
                    fe.lt(alpha*magn, delta), alpha*fe.inner(U_trial, U_test),
                    delta/magn * ( fe.inner(U_trial, U_test) - \
                                   0.5*(fe.inner(U_trial, S)*fe.inner(U, U_test) +
                                        fe.inner(U_trial, U)*fe.inner(S, U_test))/magn )
           )
    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        hess = hess + reg*fe.inner(U_trial, U_test)
    if dx is not None:
        return (hess)*dx
    return (hess)*fe.dx

def updateNewtonPerturbed(S, S_test, U, U_step, alpha, delta, dx=None):
    '''
    Creates the weak form for updating the dual variable of the perturbed
    Newton linearization:

        \int (U/|U|,S_test) + 1/|U|*(I - U(x)S/|U|)*(U_step,S_test)
    '''
    magn = fe.sqrt(fe.inner(U, U))
    #dual = fe.conditional(
    #        fe.lt(alpha*magn, delta), 0.0*fe.inner(U, S_test),
    #        fe.inner(U, S_test)/magn + \
    #        ( fe.inner(U_step, S_test) - \
    #          fe.inner(U_step, S)*fe.inner(U, S_test)/magn ) / magn
    #)
    dual = fe.conditional(
            fe.lt(alpha*magn, delta),
            alpha/delta*(fe.inner(U, S_test)+fe.inner(U_step, S_test)), \
            1.0/magn*(fe.inner(U, S_test)+fe.inner(U_step, S_test)) - \
            fe.inner(U_step, S)*fe.inner(U, S_test)/(magn*magn)
    )

    if dx is not None:
        return (dual)*dx
    return (dual)*fe.dx

def dualstepNewtonPerturbed(S, S_test, U, U_step, alpha, delta, dx=None):
    '''
    Creates the weak form for dual variable step of the perturbed
    Newton linearization:

        \int (U/|U|,S_test) + 1/|U|*(I - U(x)S/|U|)*(U_step,S_test)
    '''
    magn = fe.sqrt(fe.inner(U, U))
    #dual = fe.conditional(
    #        fe.lt(alpha*magn, delta), 0.0*fe.inner(U, S_test),
    #        fe.inner(U, S_test)/magn + \
    #        ( fe.inner(U_step, S_test) - \
    #          fe.inner(U_step, S)*fe.inner(U, S_test)/magn ) / magn
    #)
    dual = fe.conditional(
            fe.lt(alpha*magn, delta),
            alpha/delta*(fe.inner(U, S_test)+fe.inner(U_step, S_test)), \
            (fe.inner(U, S_test)+fe.inner(U_step, S_test))/magn - \
            fe.inner(U_step, S)*fe.inner(U, S_test)/(magn*magn)
    )
    dual = dual - fe.inner(S, S_test)

    if dx is not None:
        return (dual)*dx
    return (dual)*fe.dx

#=======================================
# Basic Weak Forms
#=======================================

def mass(FncSp):
    '''
    Creates the weak form for a mass matrix.
    '''
    return fe.inner(fe.TrialFunction(FncSp), fe.TestFunction(FncSp)) * fe.dx

def magnitude(U, FncSpScalar):
    '''
    Creates the weak form for computing a pointwise magnitude.
    '''
    return fe.sqrt(fe.inner(U, U)) * fe.TestFunction(FncSpScalar) * fe.dx

def magnitude_normalize(U, FncSp):
    '''
    Creates the weak form for normalizing a function by its magnitude.
    '''
    magn     = fe.sqrt(fe.inner(U, U))
    magn_min = 1.0e20 * sys.float_info.min
    return fe.inner( fe.conditional(fe.lt(magn_min, magn), U/magn, 0.0*U),
                     fe.TestFunction(FncSp) ) * fe.dx

def magnitude_scale(U_magn, U_scal, FncSp):
    '''
    Creates the weak form for scaling a function by the magnitude of another function.
    '''
    magn = fe.sqrt(fe.inner(U_magn, U_magn))
    return fe.inner( fe.conditional(fe.lt(1.0, magn), magn*U_scal, U_scal),
                     fe.TestFunction(FncSp) ) * fe.dx
