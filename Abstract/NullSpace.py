'''
=======================================
Provides functionalities for null spaces.

Author:             Johann Rudi
=======================================
'''

import fenics as fe
import numpy as np
import math

import Abstract.Vector

def convertToBasis(null, const):
    ''' Normalizes a function with respect to the norm of its function space. '''
    # compute norm
    norm_val = math.sqrt(fe.assemble(fe.inner(const, null)*fe.dx))
    assert np.isfinite(1.0/norm_val)
    # normalize
    Abstract.Vector.scale(null, 1.0/norm_val)

def projectOut(u, nullspace_basis):
    ''' Projects out the null space described by a basis. '''
    assert isinstance(nullspace_basis, list)
    for null in nullspace_basis:
        null_comp = fe.assemble(fe.inner(u, null)*fe.dx)
        u.vector().axpy(-null_comp, null.vector())

def projectOutRhs(r, FncSp, nullspace_basis):
    ''' Projects out the null space described by a basis. '''
    assert isinstance(nullspace_basis, list)
    for null in nullspace_basis:
        #TODO this might have a bug:
        null_comp = r.inner(null.vector())
        null_mass = fe.assemble(fe.inner(null, fe.TestFunction(FncSp))*fe.dx)
        r.axpy(-null_comp, null_mass)
