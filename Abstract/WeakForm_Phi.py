'''
=======================================
Defines abstract weak forms.

Author:             Johann Rudi
                    Melody Shih
=======================================
'''

import fenics as fe
import math
import sys

#=======================================
# Objective
#=======================================

def objective(U, Phi, reg=None, dx=None):
    '''
    Creates the weak form for the objetive functional:

        \int reg/2*|U|^2 + Phi
    '''
    obj = Phi

    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        obj = obj + 0.5*reg*fe.inner(U, U)

    if dx is not None:
        return (obj)*dx

    return (obj)*fe.dx

#=======================================
# Linearization
#=======================================

def gradient(U, U_test, dPhi, reg=None, dx=None):
    '''
    Creates the weak form for the gradient:

        \int reg*(U,U_test) + dPhi(sigma)/(2*sigma)*(U,U_test)
    '''
    sigma = fe.sqrt(fe.inner(0.5*U, U))
    grad = dPhi/(2*sigma)*fe.inner(U, U_test)

    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        grad = grad + reg*fe.inner(U, U_test)
    if dx is not None:
        return (grad)*dx
    return (grad)*fe.dx

def hessian_Picard(U, U_trial, U_test, dPhi, reg=None, dx=None):
    '''
    Creates the weak form for the Hessian of the Picard linearization:

        \int reg*(U_trial,U_test) + dPhi/(2*sigma)*(U_trial,U_test)
    '''
    sigma = fe.sqrt(fe.inner(0.5*U, U))
    hess = dPhi/(2*sigma)*fe.inner(U_trial, U_test)

    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        hess = hess + reg*fe.inner(U_trial, U_test)
    if dx is not None:
        return (hess)*dx
    return (hess)*fe.dx

def hessian_NewtonStandard(U, U_trial, U_test, dPhi, dsqPhi, reg=None, dx=None):
    '''
    Creates the weak form for the Hessian of the standard Newton linearization:

        \int reg*(Utrial,U_test) + 
             dPhi/(2*sigma)*(I - (dPhi-sigma*dsqPhi)/dPhi * U(x)U/|U|^2)*
                                                               (Utrial, U_test)

    '''
    sigma = fe.sqrt(fe.inner(0.5*U, U))
    magn_sq = fe.inner(U, U)
    hess = dPhi/(2*sigma)*(fe.inner(U_trial, U_test) - \
           (dPhi-sigma*dsqPhi)/dPhi* \
           fe.inner(U_trial, U)*fe.inner(U, U_test)/magn_sq)

    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        hess = hess + reg*fe.inner(U_trial, U_test)

    if dx is not None:
        return (hess)*dx
    return (hess)*fe.dx

def hessian_NewtonStressvel(U, U_trial, U_test, S, dPhi, dsqPhi, 
    scale=1.0, reg=None, dx=None):
    '''
    Creates the weak form for the Hessian of the stress-vel Newton 
    linearization:

        \int reg*(Utrial,U_test) + 
             dPhi/(2*sigma)*(I - (dPhi-sigma*dsqPhi)/dPhi * sqrt(2)/dPhi(sigma)
                                         * U(x)S/|U|)*(Utrial, U_test)
    '''
    sigma = fe.sqrt(fe.inner(0.5*U, U))
    magn = fe.sqrt(fe.inner(U, U))
    hess = dPhi/(2*sigma)*(fe.inner(U_trial, U_test) - \
           (dPhi-sigma*dsqPhi)/dPhi*math.sqrt(2)/dPhi\
                *scale*fe.inner(U_trial, S)*fe.inner(U, U_test)/magn)
        
    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        hess = hess + reg*fe.inner(U_trial, U_test)
    if dx is not None:
        return (hess)*dx
    return (hess)*fe.dx

def hessian_NewtonStressvelSym(U, U_trial, U_test, S, dPhi, dsqPhi, 
    scale=1.0, reg=None, dx=None):
    '''
    Creates the weak form for the Hessian of the standard Newton linearization:

        \int reg*(Utrial,U_test) + 
             dPhi/(2*sigma)*(I - (dPhi-sigma*dsqPhi)/dPhi * sqrt(2)/dPhi(sigma)
                                          * (U(x)S+S(x)U)/|U|)*(Utrial, U_test)
    '''
    sigma = fe.sqrt(fe.inner(0.5*U, U))
    magn = fe.sqrt(fe.inner(U, U))
    hess = dPhi/(2*sigma)*(fe.inner(U_trial, U_test) - \
          (dPhi-sigma*dsqPhi)/dPhi*math.sqrt(2)/dPhi\
               *scale*0.5*(fe.inner(U_trial, S)*fe.inner(U, U_test)+
                           fe.inner(U_trial, U)*fe.inner(S, U_test))/magn)

    if reg is not None:
        assert isinstance(reg, float) and 0.0 <= reg
        hess = hess + reg*fe.inner(U_trial, U_test)
    if dx is not None:
        return (hess)*dx
    return (hess)*fe.dx

def dualstepNewtonStressvel(S, S_test, U, U_step, dPhi, dsqPhi, 
    scale=1.0, dx=None):
    '''
    Creates the weak form for dual variable step of the stress-vel
    Newton linearization:

       \int -(S, S_test) + 
            dPhi/(2*sigma) * ((U_step,S_test) + (U, S_test)) + 
            -(dPhi-sigma*dsqPhi)/(dPhi*sigma) * U(x)S/(2*sigma)*(U_step,S_test)
    '''
    sigma = fe.sqrt(fe.inner(0.5*U, U))
    if isinstance(scale, float):
        if scale > 1e15:
            dual = 0.0
        else:
            dual = dPhi/(2*sigma*scale)*fe.inner(U_step, S_test) + \
                   dPhi/(2*sigma*scale)*fe.inner(U, S_test) - \
                    (dPhi-sigma*dsqPhi)/(dPhi*sigma)* \
                    fe.inner(U_step, S)*fe.inner(U, S_test)/(2*sigma)
    else:
        dual = dPhi/(2*sigma*scale)*fe.inner(U_step, S_test) + \
               dPhi/(2*sigma*scale)*fe.inner(U, S_test) - \
               (dPhi-sigma*dsqPhi)/(dPhi*sigma)* \
               fe.inner(U_step, S)*fe.inner(U, S_test)/(2*sigma)
    dual = dual - fe.inner(S, S_test)

    if dx is not None:
        return (dual)*dx
    return (dual)*fe.dx

def dualResidual(S, S_test, U, dPhi, scale=1.0, dx=None):
    '''
    Creates the weak form for residual of dual variable:
  
        \int dPhi/(2*sigma)*(U,S_test) - (S,S_test)
    '''
    sigma = fe.sqrt(fe.inner(0.5*U, U))
    dualres = fe.inner(U, S_test)/scale*dPhi/(2*sigma)
    dualres -= fe.inner(S, S_test)

    if dx is not None:
        return (dualres)*dx
    return (dualres)*fe.dx
