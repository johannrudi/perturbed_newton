'''
=======================================
Defines elemental bits.

Author:             Johann Rudi
=======================================
'''

import fenics as fe

#def _min(a, b):
#    ''' Creates the min operator. '''
#    return 0.5 * (a + b - abs(a-b))

#def _max(a, b):
#    ''' Creates the max operator. '''
#    return 0.5 * (a + b + abs(a-b))

def enforceBounds(val, vmin=None, vmax=None):
    ''' Enforces bounds from below and/or above (without integral). '''
    if vmin is not None and vmax is not None:
        return fe.conditional( fe.lt(val, vmin), fe.Constant(vmin), 
                               fe.conditional(fe.le(val, vmax), val, fe.Constant(vmax)) )
    elif vmin is not None and vmax is None:
        return fe.conditional(fe.lt(val, vmin), fe.Constant(vmin), val)
    elif vmin is None and vmax is not None:
        return fe.conditional(fe.le(val, vmax), val, fe.Constant(vmax))
    else:
        raise ValueError("No bounds, vmin or vmax, are provided.")
