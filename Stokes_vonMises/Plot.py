'''
=======================================
Creates plots.

Author:             Johann Rudi
                    Melody Shih
=======================================
'''

import fenics as fe
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm
import sys

sys.path.append("..")
import Abstract.Vector

PHI_CASE = "Ideal"
class Plotter(object):
    def __init__(self, mesh, display=True, writeFileType='png', writeDPI=100):
        self.mesh = mesh
        self.display = display
        self.writeFileType = writeFileType
        self.writeDPI = int(writeDPI)


    #======================================

    title_mesh      = 'Mesh'
    title_viscosity = 'Viscosity'
    title_rhs       = 'Right-hand side'
    title_yielding_marker  = 'Yielding marker'
    title_sol_velocity     = 'Solution: velocity'
    title_sol_pressure     = 'Solution: pressure'
    title_sol_viscosity    = 'Effective viscosity'
    title_sol_strainrateII = 'Solution: 2nd invariant of the strain rate'
    title_residual_mom  = 'Residual: momentum'
    title_residual_mass = 'Residual: mass'
    title_step_vel   = 'Step: velocity'
    title_step_press = 'Step: pressure'

    def plottingActivated(self):
        return (self.display or self.writeFileType is not None)

    def plotSetup(self, visc, visc_min, visc_max, rhs, visc2=None, 
                  submesh1=None, submesh2=None, viscscale=1.0):
        ''' Plots variables that were generated during setup. '''
        # return if nothing to do
        if not self.plottingActivated():
            return

        # plot mesh; write to file
        plt.figure()
        pltret = fe.plot(self.mesh, title=Plotter.title_mesh)
        if self.writeFileType is not None:
            plt.savefig("setup_mesh."+self.writeFileType, dpi=self.writeDPI)

        # plot viscosity
        plt.figure()
        if visc2 is not None:
            pltret = fe.plot(visc*viscscale, mesh=submesh1, 
                     title=Plotter.title_viscosity,
                     norm=colors.SymLogNorm(linthresh=visc_min*1e-3*viscscale,
                                            vmin=visc_min*viscscale,
                                            vmax=visc_max*viscscale)) 
            pltret = fe.plot(visc2*viscscale, mesh=submesh2, 
                     title=Plotter.title_viscosity,
                     norm=colors.SymLogNorm(linthresh=visc_min*1e-3*viscscale,
                                            vmin=visc_min*viscscale,
                                            vmax=visc_max*viscscale)) 
            plt.colorbar(plt.cm.ScalarMappable(
                      norm=colors.SymLogNorm(linthresh=visc_min*1e-3*viscscale,
                                             vmin=visc_min*viscscale,
                                             vmax=visc_max*viscscale)), 
                      extend='both')
        else:
            pltret = fe.plot(visc, mesh=self.mesh, title=Plotter.title_viscosity)
            pltret.set_clim(visc_min, visc_max)
            pltret.set_cmap('plasma_r')
            pltret.set_norm(colors.LogNorm(vmin=visc_min, vmax=visc_max))
        if self.writeFileType is not None:
            plt.savefig("setup_viscosity."+self.writeFileType, dpi=self.writeDPI)

        # plot right-hand side
        plt.figure()
        pltret = fe.plot(rhs, mesh=self.mesh, title=Plotter.title_rhs)
        plt.colorbar(pltret)
        if self.writeFileType is not None:
            plt.savefig("setup_rhs."+self.writeFileType, dpi=self.writeDPI)

        # display plots
        if self.display:
            plt.show()

        # close all plots
        plt.close('all')


    def plotSolution(self, solution, strainrateII=None, visc=None, 
                     visc_min=None, visc_max=None, yield_strength=None, 
                     visc2=None, submesh1=None, submesh2=None, viscscale=1.0, 
                     velscale=1.0, strainrateIIscale=1.0):
        ''' Plots the solution. '''
        # return if nothing to do
        if not self.plottingActivated():
            return

        # get velocity and pressure (shallow copy)
        (u, p) = solution.split()

        # plot velocity
        plt.figure()
        pltret = fe.plot(u*velscale, self.mesh, title=Plotter.title_sol_velocity)
       #pltret.set_clim(0.0, 1.0)
        plt.colorbar(pltret)
        if self.writeFileType is not None:
            plt.savefig("solution_velocity."+self.writeFileType, 
                        dpi=self.writeDPI)

        # plot pressure
        plt.figure()
        pltret = fe.plot(p, self.mesh, title=Plotter.title_sol_pressure)
        plt.colorbar(pltret)
        if self.writeFileType is not None:
            plt.savefig("solution_pressure."+self.writeFileType, 
                        dpi=self.writeDPI)

        # plot strain rate
        if strainrateII is not None:
            plt.figure()
            if strainrateIIscale is not None:
                pltret = fe.plot(strainrateII*strainrateIIscale, mesh=self.mesh,
                    title=Plotter.title_sol_strainrateII)
                pltret.set_cmap('jet')
            else:
                pltret = fe.plot(strainrateII, mesh=self.mesh,
                    title=Plotter.title_sol_strainrateII)
                pltret.set_cmap('jet')
            plt.colorbar(pltret)
            if self.writeFileType is not None:
                plt.savefig("solution_strainrateII."+self.writeFileType, 
                            dpi=self.writeDPI)

        # plot viscosity
        if visc is not None and \
           strainrateII is not None and \
           yield_strength is not None:
            if PHI_CASE == 'Ideal':
                visc_eff = visc_min + fe.conditional(
                                  fe.lt(2.0*visc*strainrateII, yield_strength),
                                  visc, 0.5*yield_strength/strainrateII)
            elif PHI_CASE == 'Comp':
                visc_eff = yield_strength*visc/(2*strainrateII*visc+yield_strength)
            else: 
                raise ValueError("Unknown type of Phi")
            visc_eff = visc_eff*viscscale
            plt.figure()
            if visc2 is not None:
                pltret = fe.plot(visc_eff, mesh=self.mesh, 
                     norm=colors.SymLogNorm(linthresh=visc_min*1e-3*viscscale,
                                            vmin=visc_min*viscscale,
                                            vmax=visc_max*viscscale), 
                     title=Plotter.title_sol_viscosity)
                pltret = fe.plot(visc2, mesh=submesh2,
                     norm=colors.SymLogNorm(linthresh=visc_min*1e-3*viscscale,
                                            vmin=visc_min*viscscale,
                                            vmax=visc_max*viscscale),
                     title=Plotter.title_sol_viscosity)
                plt.colorbar(plt.cm.ScalarMappable(
                     norm=colors.SymLogNorm(linthresh=visc_min*1e-3*viscscale,
                                            vmin=visc_min*viscscale,
                                            vmax=visc_max*viscscale)), 
                     extend='both')
            else:
                pltret = fe.plot(visc_eff, mesh=self.mesh, 
                                 title=Plotter.title_sol_viscosity)
                pltret.set_cmap('plasma_r')
                pltret.set_norm(colors.SymLogNorm(linthresh=visc_min*1e-3,
                                vmin=visc_min,vmax=visc_max))
                plt.colorbar(pltret)
            if self.writeFileType is not None:
                plt.savefig("solution_viscosity."+self.writeFileType, 
                                                             dpi=self.writeDPI)

            yielding = fe.conditional(fe.lt(2.0*visc*strainrateII, 
                                            yield_strength),
                                      fe.Constant(0.0), fe.Constant(1.0))
            fig = plt.figure()
            if visc2 is not None:
                pltret = fe.plot(yielding, mesh=self.mesh,
                             norm=colors.Normalize(vmin=0,vmax=1),
                             title=Plotter.title_yielding_marker)
                pltret.set_cmap('gray')
                pltret = fe.plot(fe.Constant(0.0), mesh=submesh2, 
                             norm=colors.Normalize(vmin=0,vmax=1),
                             title=Plotter.title_yielding_marker)
                pltret.set_cmap('gray')
                plt.colorbar(plt.cm.ScalarMappable(norm=colors.Normalize(
                                                   vmin=0,vmax=1),cmap='gray'))
            else:
                pltret = fe.plot(yielding, mesh=self.mesh, 
                                 title=Plotter.title_yielding_marker)
                pltret.set_cmap('gray')
            if self.writeFileType is not None:
                plt.savefig("solution_yielding_marker."+self.writeFileType, 
                                                             dpi=self.writeDPI)

        # display plots
        if self.display:
            plt.show()

        # close all plots
        plt.close('all')

    def plotVectorField(self, UV, title='', writeFileBase=None, magnLimits=None, 
                        logscale=False):
        ''' Plots a vector field with a colormap as background. '''
        # return if nothing to do
        if not self.plottingActivated():
            return None

        # get mesh coordinates
        Coord = self.mesh.coordinates()
        X, Y = Coord[:,0], Coord[:,1]

        # get data from vector
        UV_vals = Abstract.Vector.getVertexValues(UV)
        U, V = np.ravel(UV_vals[:,0]), np.ravel(UV_vals[:,1])

        # compute magnitude
        M = np.sqrt(U**2 + V**2)
        if magnLimits is not None:
            M = np.clip(M, magnLimits[0], magnLimits[1])

        # plot
        fig, ax = plt.subplots()
        qui = ax.quiver(X, Y, U, V, M)

        # set limits and scale

        # format plot
        ax.set_title(title)
        ax.set_aspect('equal')
        if magnLimits is not None:
            qui.set_clim(magnLimits[0], magnLimits[1])
        qui.set_cmap('coolwarm')
        if logscale:
            if magnLimits is None:
                magnLimits = (M.min(), M.max())
            colornorm = colors.LogNorm(vmin=magnLimits[0], vmax=magnLimits[1])
            qui.set_norm(colornorm)
        plt.colorbar(qui)

        # write to file
        if writeFileBase is not None:
            plt.savefig(writeFileBase+'.'+self.writeFileType, dpi=self.writeDPI)

        # display plots
        if self.display:
            plt.show()

        return qui

    def plotVectorField_colorBack(self, UV, title='', writeFileBase=None, 
                                  magnLimits=None, logscale=False):
        ''' Plots a vector field with a colormap as background. '''
        # return if nothing to do
        if not self.plottingActivated():
            return None

        # get mesh coordinates
        Coord = self.mesh.coordinates()
        X, Y = Coord[:,0], Coord[:,1]

        # get vertex values from vector
        UV_vals = Abstract.Vector.getVertexValues(UV)
        U, V = np.ravel(UV_vals[:,0]), np.ravel(UV_vals[:,1])

        # compute magnitude
        M = np.sqrt(U**2 + V**2)
        if magnLimits is not None:
            M = np.clip(M, magnLimits[0], magnLimits[1])
        n_entries_per_dim = int(np.sqrt(M.size))
        M_sq = np.reshape(M, (n_entries_per_dim, n_entries_per_dim))

        # plot
        fig, ax = plt.subplots()
        img = plt.imshow(M_sq, extent=[np.min(X),np.max(X),np.min(Y),np.max(Y)])
        qui = ax.quiver(X, Y, U, V)

        # set limits and scale

        # format plot
        ax.set_title(title)
        ax.set_aspect('equal')
        if magnLimits is not None:
            qui.set_clim(magnLimits[0], magnLimits[1])
        img.set_cmap('plasma')
        if logscale:
            if magnLimits is None:
                magnLimits = (M.min(), M.max())
            colornorm = colors.LogNorm(vmin=magnLimits[0], vmax=magnLimits[1])
            img.set_norm(colornorm)
        plt.colorbar(img)

        # write to file
        if writeFileBase is not None:
            plt.savefig(writeFileBase+'.'+self.writeFileType, dpi=self.writeDPI)

        # display plots
        if self.display:
            plt.show()

        return qui, img

    #======================================

    solstats_title_Gu    = 'grad(u)'
    solstats_title_Gu_TV = 'grad(u)/TV(u)'
    solstats_title_S     = 'Dual variable'
    solstats_title_E     = 'Model error'
    solstats_title_D     = 'Model perturbation'

    solstats_filebase_Gu    = 'solver_stats_Gu'
    solstats_filebase_Gu_TV = 'solver_stats_Gu_TV'
    solstats_filebase_S     = 'solver_stats_S'
    solstats_filebase_E     = 'solver_stats_E'
    solstats_filebase_D     = 'solver_stats_D'

    def plotSolverStatistics(self, itn, gradient, step):
        ''' Plots some solver statistics. '''
        # return if nothing to do
        if not self.plottingActivated():
            return

        # get velocity and pressure (shallow copy)
        (grad_u, grad_p) = gradient.split()
        (step_u, step_p) = step.split()

        # plot residual of momentum
        plt.figure()
        pltret = fe.plot(grad_u, self.mesh, 
                      title=Plotter.title_residual_mom + " (iteration %i)"%itn)
       #pltret.set_clim(0.0, 1.0)
        plt.colorbar(pltret)
        if self.writeFileType is not None:
            plt.savefig("stats_residual_mom_it%03i.%s"%(itn, self.writeFileType),
                        dpi=self.writeDPI)

        # plot residual of mass
        plt.figure()
        pltret = fe.plot(grad_p, self.mesh, 
                     title=Plotter.title_residual_mass + " (iteration %i)"%itn)
       #pltret.set_clim(0.0, 1.0)
        plt.colorbar(pltret)
        if self.writeFileType is not None:
            plt.savefig("stats_residual_mass_it%03i.%s"%(itn, self.writeFileType), 
                        dpi=self.writeDPI)

        # plot step velocity
        plt.figure()
        pltret = fe.plot(step_u, self.mesh, 
                          title=Plotter.title_step_vel + " (iteration %i)"%itn)
       #pltret.set_clim(0.0, 1.0)
        plt.colorbar(pltret)
        if self.writeFileType is not None:
            plt.savefig("stats_step_vel_it%03i.%s"%(itn, self.writeFileType), 
                        dpi=self.writeDPI)

        # plot step pressure
        plt.figure()
        pltret = fe.plot(step_p, self.mesh, 
                         title=Plotter.title_step_press + " (iteration %i)"%itn)
       #pltret.set_clim(0.0, 1.0)
        plt.colorbar(pltret)
        if self.writeFileType is not None:
            plt.savefig("stats_step_press_it%03i.%s"%(itn, self.writeFileType), 
                        dpi=self.writeDPI)

        # close all plots
        plt.close('all')
