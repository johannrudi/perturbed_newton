#!/bin/zsh
gmsh -2 -algo del2d compression_mesh_rounded_refine.geo
## uncomment following line when run with Discretization.HIGH_ORDER_DISC_PRESS 
#gmsh -barycentric_refine compression_mesh_rounded_refine.msh
meshio-convert compression_mesh_rounded_refine.msh compression_mesh_rounded_refine.xdmf -p -z
python CreateXdmlfile.py
