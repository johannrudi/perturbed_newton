''' =======================================
Runs nonlinear Stokes solver for Example2

Author:             Johann Rudi
                    Melody Shih
=======================================
'''

from fenics import *
from dolfin import *
import numpy as np
import scipy.sparse
import mshr  # mesh generation component of FEniCS

import StokesProblem
import WeakForm
import Plot

import sys
sys.path.append("..")
import Abstract.WeakForm_Phi
import Abstract.Vector
import Abstract.NullSpace
import Abstract.CheckDerivatives

from pprint import pprint

#======================================
# Constants / Enumerators
#======================================

from enum import Enum, auto

class Linearization(Enum):
    PICARD = auto()
    NEWTON_STANDARD = auto() 
    NEWTON_STRESSVEL = auto() 
    NEWTON_STRESSVEL_SYM = auto()

    def name(linearization):
        if Linearization.PICARD == linearization:
            return 'Picard'
        elif Linearization.NEWTON_STANDARD == linearization:
            return 'Newton w/ standard lin.'
        elif Linearization.NEWTON_STRESSVEL == linearization:
            return 'Newton w/ stress-vel lin.'
        elif Linearization.NEWTON_STRESSVEL_SYM == linearization: 
            return 'Newton w/ stress-vel & sym. lin.' 
        else:
            return '[Unknown type of linearization]'

class Discretization(Enum):
    HIGH_ORDER_TAYLOR_HOOD = auto()

    def name(discretization):
        if Discretization.HIGH_ORDER_TAYLOR_HOOD == discretization:
            return 'Stable, high-order Taylor-Hood F.E.'
        else:
            return '[Unknown type of linearization]'

class Plasticitymodel(Enum):
    VON_MISES = auto()
    DEPTH_DEPENDENT_VON_MISES = auto() 

    def name(plasticitymodel):
        if Plasticitymodel.VON_MISES == plasticitymodel:
            return 'Von Mises'
        elif Plasticitymodel.DEPTH_DEPENDENT_VON_MISES == plasticitymodel:
            return 'Depth dependent Von Mises'
        else:
            return '[Unknown type of plasticity model]'

#======================================
# Parameters
#======================================
parameters["form_compiler"]["quadrature_degree"] = 10

# discretization parameters
GEOMETRY_LENGTH = 4.0
GEOMETRY_HEIGHT = 2.0
GEOMETRY_CYLIDER_RADIUS = 0.1  # radius of cylinder or `None`
MESH_CYLIDER_N_SEGMENTS = 64 #10
MESH_N = 150 #20

DISCR = Discretization.HIGH_ORDER_TAYLOR_HOOD
DISCR_FNCSP_VEL_DEGREE = 2
DISCR_FNCSP_VEL_DUAL_FAMILY = 'DG'
DISCR_FNCSP_VEL_DUAL_DEGREE = max(0, DISCR_FNCSP_VEL_DEGREE - 1)

# Stokes problem parameters
VISCOSITY_TYPE = 'const' # const, anomaly
VISCOSITY_MIN  = 1.e-5
VISCOSITY_MAX  = 1.e3

RHO = 2700.0
GRAVITY = 9.81
YEAR_PER_SEC = 1./3600/365.25/24

REFERENCE_VELOCITY = 2.5e-3
REFERENCE_HEIGHT      = 30000.0 #1.0
REFERENCE_VISCOSITY   = 1.e22
REFERENCE_STRAIN_RATE = REFERENCE_VELOCITY* \
                        YEAR_PER_SEC/REFERENCE_HEIGHT
REFERENCE_STRESS_RATE = 2.0*REFERENCE_STRAIN_RATE*REFERENCE_VISCOSITY

# rescaled parameter
VISC_RECT = 1.e24/REFERENCE_VISCOSITY
VISC_CIRC = 1.e17/REFERENCE_VISCOSITY
BDRY_VEL_SCALED = 1.e-3/REFERENCE_VELOCITY

L2_on_H2 = REFERENCE_STRAIN_RATE*REFERENCE_VISCOSITY/RHO/GRAVITY/REFERENCE_HEIGHT

PLASTICMODEL = Plasticitymodel.VON_MISES

RHS_TYPE = 'constvec'  # zero, const, anomaly
RHS_SCALINGVEC = [0.0,0.0]

# linearization type
LINEARIZATION = Linearization.NEWTON_STRESSVEL

# nonlinear solver
NL_SOLVER_MAXITER        = 300
NL_SOLVER_GRAD_RTOL      = 1.0e-6
NL_SOLVER_GRAD_STEP_RTOL = 1.0e-10
NL_SOLVER_STEP_MAXITER   = 15
NL_SOLVER_STEP_ARMIJO    = 1.0e-4

# checks for gradients and Hessians
NL_CHECK_GRADIENT = False  # True/False
NL_CHECK_HESSIAN  = False  # True/False

# file output
OUTPUT_FILETYPE     = 'png'  # None or file type
OUTPUT_SOLVER_STATS = False  # True/False
OUTPUT_NL_CONV_STATS = False # True/False
OUTPUT_VTK           = False
OUTPUT_FILENAME = './results/T_picard_huber_vm_0227'

# screen output
MONITOR_NL_ITER       = True   # True/False
MONITOR_NL_LINESEARCH = False  # True/False
MONITOR_L_ITER        = False  # True/False
DISPLAY_PLOTS         = False  # True/False

#======================================
# Advanced Parameters
#======================================

DISCR_STABILIZATION_FACTOR = 0.5

# linear solver (sub-solver within nonlinear solver loop)
L_SOLVER_TYPE    = 'default'  # see iterative method with: list_linear_solver_methods()
L_SOLVER_PC_TYPE = 'default'  # see preconditioner with: list_krylov_solver_preconditioners()
L_SOLVER_MAXITER = 100
L_SOLVER_RTOL    = 1.0e-8

# linear solver for approximately inverting mass matrices
MASS_L_SOLVER_TYPE    = 'cg' #cg
MASS_L_SOLVER_PC_TYPE = 'jacobi' #jacobi
MASS_L_SOLVER_MAXITER = 100
MASS_L_SOLVER_RTOL    = 1.0e-8

#======================================
# Setup Discretization
#======================================

# create mesh
pt_bl  = Point(-0.5*GEOMETRY_LENGTH, -GEOMETRY_HEIGHT) 
pt_tr  = Point(pt_bl[0]+GEOMETRY_LENGTH, pt_bl[1]+GEOMETRY_HEIGHT)
pt_mid = Point(0.0, -0.5*GEOMETRY_HEIGHT)

class Circle(SubDomain):
    def inside(self, x, on_boundary):
        return (x[0]*x[0] + \
               (x[1]+0.5*GEOMETRY_HEIGHT)*(x[1]+0.5*GEOMETRY_HEIGHT))\
               <= GEOMETRY_CYLIDER_RADIUS*GEOMETRY_CYLIDER_RADIUS+DOLFIN_EPS

# create mesh on a rectangular domain with a circular hole in the middle
circle = mshr.Circle(pt_mid, GEOMETRY_CYLIDER_RADIUS, MESH_CYLIDER_N_SEGMENTS)
rectangle = mshr.Rectangle(pt_bl, pt_tr)

domain = rectangle
domain.set_subdomain(1, circle)
domain.set_subdomain(2, rectangle - circle)
mesh = mshr.generate_mesh(domain, MESH_N)

#cell_markers = MeshFunction("bool", mesh, dim=2)
#cell_markers.set_all(True)
#mesh = refine(mesh, cell_markers)

epsilon = 1
cell_markers = MeshFunction("bool", mesh, dim=2)
cell_markers.set_all(False)
for cell in cells(mesh):
    coo = cell.get_vertex_coordinates()
    for i in range(3):
        if ( coo[2*i]*coo[2*i] + \
            (coo[2*i+1]+0.5*GEOMETRY_HEIGHT)*\
            (coo[2*i+1]+0.5*GEOMETRY_HEIGHT) < \
            GEOMETRY_CYLIDER_RADIUS*GEOMETRY_CYLIDER_RADIUS*4):
            cell_markers[cell] = True
mesh = refine(mesh, cell_markers)

cell_markers = MeshFunction("bool", mesh, dim=2)
cell_markers.set_all(False)
for cell in cells(mesh):
    coo = cell.get_vertex_coordinates()
    for i in range(3):
        dis = coo[2*i]*coo[2*i]+(coo[2*i+1]+0.5*GEOMETRY_HEIGHT)* \
                (coo[2*i+1]+0.5*GEOMETRY_HEIGHT)
        if ( dis < GEOMETRY_CYLIDER_RADIUS*GEOMETRY_CYLIDER_RADIUS*2.25 \
             and dis > GEOMETRY_CYLIDER_RADIUS*GEOMETRY_CYLIDER_RADIUS*0.25):
            cell_markers[cell] = True
mesh = refine(mesh, cell_markers)

cell_markers = MeshFunction("bool", mesh, dim=2)
cell_markers.set_all(False)
for cell in cells(mesh):
    coo = cell.get_vertex_coordinates()
    for i in range(3):
        dis = coo[2*i]*coo[2*i]+(coo[2*i+1]+0.5*GEOMETRY_HEIGHT)* \
                (coo[2*i+1]+0.5*GEOMETRY_HEIGHT)
        if ( dis < GEOMETRY_CYLIDER_RADIUS*GEOMETRY_CYLIDER_RADIUS*(1.2656)\
            and dis > GEOMETRY_CYLIDER_RADIUS*GEOMETRY_CYLIDER_RADIUS*0.7656):
            cell_markers[cell] = True
mesh = refine(mesh, cell_markers)


# create subdomain
subdomains = MeshFunction("size_t", mesh, 2)
subdomains.set_all(2)
circle = Circle()
circle.mark(subdomains, 1)
dx = Measure('dx', subdomain_data=subdomains, domain=mesh)
dx_circ = dx(1) 
dx_rect = dx(2)

submesh_circ    = SubMesh(mesh, subdomains, 1)
submesh_rect = SubMesh(mesh, subdomains, 2)

# create finite elements
if Discretization.HIGH_ORDER_TAYLOR_HOOD == DISCR:
    weak_stab = None
    V1_e = FiniteElement('Lagrange', mesh.ufl_cell(), DISCR_FNCSP_VEL_DEGREE)
    V_e  = VectorElement(V1_e)
    Q_e  = FiniteElement('Lagrange', mesh.ufl_cell(), DISCR_FNCSP_VEL_DEGREE - 1)
    Vd1_e = FiniteElement('DG', mesh.ufl_cell(), 1)
else:
    raise ValueError("Unknown type of discretization %i" % DISCR)

# create function spaces
V1   = FunctionSpace(mesh, V1_e)
V    = FunctionSpace(mesh, V_e)
Q    = FunctionSpace(mesh, Q_e)
VQ   = FunctionSpace(mesh, V_e * Q_e)
Vd1  = FunctionSpace(mesh, Vd1_e)
Vd_e = TensorElement(DISCR_FNCSP_VEL_DUAL_FAMILY, mesh.ufl_cell(), 
    DISCR_FNCSP_VEL_DUAL_DEGREE)
Vd   = FunctionSpace(mesh, Vd_e)

# create assigner for velocity and pressure
assigner_vel_press_split   = FunctionAssigner([V,Q], VQ)
assigner_vel_press_combine = FunctionAssigner(VQ, [V,Q])

#======================================
# Setup Stokes Problem
#======================================
# set boundary conditions
#NEW:
boundary_cpp_code = {
    "left" :  "on_boundary && near(x[0], %g)" % float(-GEOMETRY_LENGTH/2.0),
    "right":  "on_boundary && near(x[0], %g)" % float(GEOMETRY_LENGTH/2.0),
    "up"   :  "on_boundary && near(x[1], 0.0)",
    "down" :  "on_boundary && near(x[1], %g)" % float(-GEOMETRY_HEIGHT)
}

# construct boundary conditions
bc_left  = DirichletBC(VQ.sub(0).sub(0), BDRY_VEL_SCALED, boundary_cpp_code["left"])
bc_right = DirichletBC(VQ.sub(0).sub(0),-BDRY_VEL_SCALED, boundary_cpp_code["right"])
bc_down  = DirichletBC(VQ.sub(0).sub(1),-0.5*BDRY_VEL_SCALED, boundary_cpp_code["down"])
bc = [bc_left, bc_right, bc_down]

# construct homogeneous Dirichlet BC's at left, right boundary for Newton steps
bc_step_left  = DirichletBC(VQ.sub(0).sub(0),0.0, boundary_cpp_code["left"])
bc_step_right = DirichletBC(VQ.sub(0).sub(0),0.0, boundary_cpp_code["right"])
bc_step_down  = DirichletBC(VQ.sub(0).sub(1),0.0, boundary_cpp_code["down"])
bc_step = [bc_step_left, bc_step_right, bc_step_down]

# set viscosity
visc_rect = StokesProblem.createViscosity(VISCOSITY_TYPE, VISC_RECT, 
                                          VISCOSITY_MIN, VISCOSITY_MAX)
visc_circ = StokesProblem.createViscosity(VISCOSITY_TYPE, VISC_CIRC, 
                                          VISCOSITY_MIN, VISCOSITY_MAX)

# set right-hand side
rhs = StokesProblem.createRightHandSide(RHS_TYPE, scalingvec=RHS_SCALINGVEC)

# plot setup variables
plotter = Plot.Plotter(mesh, DISPLAY_PLOTS, OUTPUT_FILETYPE)
plotter.plotSetup(visc_rect, VISCOSITY_MIN, VISCOSITY_MAX, rhs, visc_circ, 
  submesh_rect, submesh_circ, viscscale=REFERENCE_VISCOSITY)

#======================================
# Setup Weak Forms
#======================================

# create vectors
sol, sol_prev, step    = Function(VQ), Function(VQ), Function(VQ)
sol_u, sol_p           = Function(V), Function(Q)
sol_prev_u, sol_prev_p = Function(V), Function(Q)
step_u, step_p         = Function(V), Function(Q)

# initialize dual variable
S = None
S_prev = None
S_step = None
S_proj = None

# set weak forms of yielding stregth
if Plasticitymodel.VON_MISES == PLASTICMODEL:
    phi = 0
    C = 3.e7
    A = C
    yield_strength = A/REFERENCE_VISCOSITY/REFERENCE_STRAIN_RATE
elif Plasticitymodel.DEPTH_DEPENDENT_VON_MISES == PLASTICMODEL:
    phi = 30
    C = 3.e7
    A = C*cos(2*pi*phi/360)/REFERENCE_VISCOSITY/REFERENCE_STRAIN_RATE
    B = sin(2*pi*phi/360)
    yield_strength = Expression('A - B*x[1]/L2_on_H2', A=A, B=B, L2_on_H2=L2_on_H2, degree=10)
else:
    raise ValueError("Unknown type of plasticity model %i" % PLASTICMODEL)

# set weak forms of objective functional and gradient
obj  = WeakForm.objective(sol_u, sol_p, rhs, visc_rect, VISCOSITY_MIN, 
          yield_strength, dx, dx_rect, visc_circ, dx_circ)
grad = WeakForm.gradient(sol_u, sol_p, rhs, VQ, visc_rect, VISCOSITY_MIN, 
          yield_strength, dx, dx_rect, visc_circ, dx_circ, weak_stab)

# set weak form of Hessian and forms related to the linearization
if Linearization.PICARD == LINEARIZATION:
    (hess,hess_precond) = WeakForm.hessian_Picard(
        sol_u, sol_p, VQ, visc_rect, VISCOSITY_MIN, yield_strength, 
        dx, dx_rect, visc_circ, dx_circ)
elif Linearization.NEWTON_STANDARD == LINEARIZATION:
    (hess,hess_precond) = WeakForm.hessian_NewtonStandard(
        sol_u, sol_p, VQ, visc_rect, VISCOSITY_MIN, yield_strength, 
        dx, dx_rect, visc_circ, dx_circ, weak_stab)
elif Linearization.NEWTON_STRESSVEL == LINEARIZATION:
    S = Function(Vd)
    S_step = Function(Vd)
    S_proj = Function(Vd)
    S_prev = Function(Vd)
    dualStep = WeakForm.hessian_dualStep(
        sol_u, step_u, S, Vd, visc_rect, VISCOSITY_MIN, yield_strength, 
        dx, dx_rect, visc_circ, dx_circ)
    dualres = WeakForm.dualresidual(S, sol_u, Vd, visc_rect, 
        VISCOSITY_MIN, yield_strength, dx, dx_rect, visc_circ, dx_circ)
    (hess,hess_precond) = WeakForm.hessian_NewtonStressvel(
        sol_u, sol_p, VQ, S_proj, visc_rect, VISCOSITY_MIN, 
        yield_strength, dx, dx_rect, visc_circ, dx_circ)
elif Linearization.NEWTON_STRESSVEL_SYM == LINEARIZATION:
    S = Function(Vd)
    S_step = Function(Vd)
    S_proj = Function(Vd)
    S_prev = Function(Vd)
    dualStep = WeakForm.hessian_dualStep(
        sol_u, step_u, S, Vd, visc_rect, visc_circ, VISCOSITY_MIN, 
        yield_strength, dx, dx_rect, dx_circ)
    dualres = WeakForm.dualresidual(S, sol_u, Vd, visc_rect, 
        VISCOSITY_MIN, yield_strength, dx, dx_rect, visc_circ, dx_circ)
    (hess,hess_precond) = WeakForm.hessian_NewtonStressvelSym(
        sol_u, sol_p, VQ, S_proj, visc_rect, VISCOSITY_MIN, 
        yield_strength, dx, dx_rect, visc_circ, dx_circ)
else:
    raise ValueError("Unknown type of linearization %i" % LINEARIZATION)

#======================================
# Setup Solver
#======================================

# initialize solution
(A,b) = WeakForm.linear_stokes(rhs, VQ, visc_rect, dx, dx_rect, visc_circ, 
                               dx_circ)
solve(A == b, sol, bc)
assigner_vel_press_split.assign([sol_u,sol_p], sol)

# initialize gradient
(_, g) = assemble_system(hess, grad, bc_step)
g_norm_init = g_norm = g.norm('l2')
dualres_norm = 0.0

angle_grad_step_init = angle_grad_step = np.nan

# initialize solver statistics
lin_it       = 0
lin_it_total = 0
obj_val      = assemble(obj)
step_length  = 0.0
if OUTPUT_SOLVER_STATS:
    g_nomass = Function(VQ)
    g_u, g_p = Function(V), Function(Q)
if OUTPUT_NL_CONV_STATS:
    f_length = open(OUTPUT_FILENAME+'_length.txt', 'w')
    f_gnorm  = open(OUTPUT_FILENAME+'.txt', 'w')
    f_obj    = open(OUTPUT_FILENAME+'_obj.txt', 'w')

# assemble mass matrices # TODO which are necessary?
Mu1   = assemble(Abstract.WeakForm.mass(V1))
Mu    = assemble(Abstract.WeakForm.mass(V))
Md    = assemble(Abstract.WeakForm.mass(Vd))
Md1 = assemble(Abstract.WeakForm.mass(Vd1))
(M,_) = assemble_system(Abstract.WeakForm.mass(VQ), grad, bc)

# create and set up linear solver for approximately inverting mass matrices
mass_lin_solver = KrylovSolver(MASS_L_SOLVER_TYPE, MASS_L_SOLVER_PC_TYPE)
mass_lin_solver.parameters["maximum_iterations"] = MASS_L_SOLVER_MAXITER
mass_lin_solver.parameters["relative_tolerance"] = MASS_L_SOLVER_RTOL
mass_lin_solver.parameters["absolute_tolerance"] = 1.0e-20
mass_lin_solver.parameters["monitor_convergence"] = False
mass_lin_solver.parameters["error_on_nonconvergence"] = True

massdual_lin_solver = KrylovSolver(MASS_L_SOLVER_TYPE, MASS_L_SOLVER_PC_TYPE)
massdual_lin_solver.parameters["maximum_iterations"] = MASS_L_SOLVER_MAXITER
massdual_lin_solver.parameters["relative_tolerance"] = MASS_L_SOLVER_RTOL
massdual_lin_solver.parameters["absolute_tolerance"] = 1.0e-20
massdual_lin_solver.parameters["monitor_convergence"] = False
massdual_lin_solver.parameters["error_on_nonconvergence"] = True
#======================================
# Solve
#======================================

# print iterations header
if MONITOR_NL_ITER:
    print("{0:<3} {1:>6}{2:^20}{3:^14}{4:^15}{5:^10}{6:^20}".format(
          "Itn", L_SOLVER_TYPE, "Energy", "||g||_l2", "(grad,step)", "step len", "dualres"))

# run Newton's method
for itn in range(NL_SOLVER_MAXITER+1):
    # print iteration line
    if MONITOR_NL_ITER:
        print("{0:>3d} {1:>6d}{2:>20.12e}{3:>14.6e}{4:>+15.6e}{5:>10f}{6:20.12e}".format(
              itn, lin_it, obj_val, g_norm, angle_grad_step, step_length, dualres_norm))
    if OUTPUT_NL_CONV_STATS:
        f_length.write('%d %10f\n' % (itn, step_length))
        f_gnorm.write('%d %14.12e\n' % (itn, g_norm))
        f_obj.write('%d %20.12e\n' % (itn, obj_val))

    # output of solver statisics
    if OUTPUT_SOLVER_STATS:
        mass_lin_solver.solve(M, g_nomass.vector(), g)
        plotter.plotSolverStatistics(itn, g_nomass, step)

    # stop if converged
    if g_norm < NL_SOLVER_GRAD_RTOL*g_norm_init:
        print("Stop reason: Converged to rtol; ||g|| reduction %3e." % float(g_norm/g_norm_init))
        break
    if np.abs(angle_grad_step) < NL_SOLVER_GRAD_STEP_RTOL*np.abs(angle_grad_step_init):
        print("Stop reason: Converged to rtol; (grad,step) reduction %3e." % \
              np.abs(angle_grad_step/angle_grad_step_init))
        break
    # stop if step search failed
    if 0 < itn and not step_success:
        print("Stop reason: Step search reached maximum number of backtracking.")
        break

    # set up the linearized system
    if Linearization.NEWTON_STRESSVEL == LINEARIZATION or \
       Linearization.NEWTON_STRESSVEL_SYM == LINEARIZATION:
        if 0 == itn:
            Abstract.Vector.setZero(S)
            Abstract.Vector.setZero(S_step)
            Abstract.Vector.setZero(S_proj)
        else:
            # project S to unit sphere
            Sprojweak = WeakForm.hessian_dualUpdate_boundMaxMagnitude(S, Vd, 1.0)
            b = assemble(Sprojweak)
            massdual_lin_solver.solve(Md, S_proj.vector(), b)

    # assemble linearized system
    (H,g) = assemble_system(hess, grad, bc_step)
    (P,_) = assemble_system(hess_precond, grad, bc_step)
    if Linearization.NEWTON_STRESSVEL == LINEARIZATION or \
       Linearization.NEWTON_STRESSVEL_SYM == LINEARIZATION:
        dres = assemble(dualres)

    # check derivatives
    if NL_CHECK_GRADIENT:
        perturb, perturb_u, perturb_p = Function(VQ), Function(V), Function(Q)

        randrhs = Function(V)
        Abstract.Vector.setZero(randrhs)
        Abstract.Vector.addNoiseRandUniform(randrhs)

        # create random direction and apply the right bcs
        Abstract.Vector.setZero(perturb)
        Abstract.Vector.addNoiseRandUniform(perturb)
        StokesProblem.applyBoundaryConditions(perturb, bc_step)

        # scale the direction so that perturb and sol have similar scale
        (M,b) = assemble_system(Abstract.WeakForm.mass(VQ),
                                Abstract.WeakForm.magnitude_scale(sol, perturb, VQ), bc_step)
        solve(M, perturb.vector(), b, MASS_L_SOLVER_TYPE, MASS_L_SOLVER_PC_TYPE)
        #StokesProblem.applyBoundaryConditions(perturb, bc_step) #TODO BC necessary?
        assigner_vel_press_split.assign([perturb_u,perturb_p], perturb)	

        if NL_CHECK_GRADIENT:
            Abstract.CheckDerivatives.gradient(g, obj, sol_u, obj_perturb=perturb_u, \
                grad_perturb=perturb, n_checks=8)
        if NL_CHECK_HESSIAN:
            Abstract.CheckDerivatives.hessian(H, obj, sol_u, obj_perturb=perturb_u, \
                hess_perturb=perturb, n_checks=6)

    # solve linearized system (note: RHS is positive gradient)
    lin_it = solve(H, step.vector(), g, L_SOLVER_TYPE, L_SOLVER_PC_TYPE)
    lin_it_total += lin_it

    # solve dual variable step
    if Linearization.NEWTON_STRESSVEL == LINEARIZATION or \
       Linearization.NEWTON_STRESSVEL_SYM == LINEARIZATION:
        Abstract.Vector.scale(step, -1.0)
        assigner_vel_press_split.assign([step_u,step_p], step)
        b = assemble(dualStep)
        massdual_lin_solver.solve(Md, S_step.vector(), b)
        Abstract.Vector.scale(step, -1.0)

    # compute the norm of the gradient
    g_norm = g.norm('l2')
    # compute the norm of the dual residual
    if Linearization.NEWTON_STRESSVEL == LINEARIZATION or \
       Linearization.NEWTON_STRESSVEL_SYM == LINEARIZATION:
        dualres_norm = dres.norm('l2')

    # compute angle between step and (negative) gradient
    angle_grad_step = -step.vector().inner(g)
    if 0 == itn:
        angle_grad_step_init = angle_grad_step

    # initialize backtracking line search
    sol_prev.assign(sol)
    assigner_vel_press_split.assign([sol_prev_u,sol_prev_p], sol_prev)
    step_length = 1.0
    step_success = False

    # run backtracking line search
    for j in range(NL_SOLVER_STEP_MAXITER):
        sol.vector().axpy(-step_length, step.vector())
        StokesProblem.applyBoundaryConditions(sol, bc) #TODO BC necessary?
        assigner_vel_press_split.assign([sol_u,sol_p], sol)
        obj_val_next = assemble(obj)
        if MONITOR_NL_LINESEARCH and 0 < j:
            print("Step search: {0:>2d}{1:>10f}{2:>20.12e}{3:>20.12e}".format(
                  j, step_length, obj_val_next, obj_val))
        if obj_val_next < obj_val + step_length*NL_SOLVER_STEP_ARMIJO*angle_grad_step:
            if Linearization.NEWTON_STRESSVEL == LINEARIZATION or \
               Linearization.NEWTON_STRESSVEL_SYM == LINEARIZATION:
                S.vector().axpy(step_length, S_step.vector())
            obj_val = obj_val_next
            step_success = True
            break
        step_length *= 0.5
        sol.assign(sol_prev)
    if not step_success:
        sol.assign(sol_prev)
        assigner_vel_press_split.assign([sol_u,sol_p], sol)
    Abstract.Vector.scale(step, -step_length)

print("%s: #iter %i, ||g|| reduction %3e, (grad,step) reduction %3e, #total linear iter %i." % \
    (
        Linearization.name(LINEARIZATION),
        itn,
        g_norm/g_norm_init,
        np.abs(angle_grad_step/angle_grad_step_init),
        lin_it_total
    )
)

#======================================
# Output
#======================================

# set the 2nd invariant of the strain rate
strainrateII = WeakForm.strainrateII(sol_u)

# output vtk file for strain rate 
if OUTPUT_VTK:
    edotp   = Function(Vd1)
    edotp_t = TestFunction(Vd1)
    solve(inner(edotp_t, (edotp - strainrateII))*dx == 0.0, edotp)
    Abstract.Vector.scale(edotp, REFERENCE_STRAIN_RATE)

    filestrainrate = File('strainrateII.pvd')
    filestrainrate << edotp
    filestrainrate = File("solution_u.pvd")
    filestrainrate << sol_u
    filestrainrate = File("solution_p.pvd")
    filestrainrate << sol_p

# plot solution
plotter.plotSolution(sol, strainrateII, visc_rect, VISCOSITY_MIN, VISCOSITY_MAX,
    yield_strength, strainrateIIscale=REFERENCE_STRAIN_RATE, visc2=visc_circ, \
    submesh1=submesh_rect, submesh2=submesh_circ, viscscale=REFERENCE_VISCOSITY, 
    velscale=REFERENCE_VELOCITY)
