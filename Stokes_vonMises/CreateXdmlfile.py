from dolfin import *
import meshio

def writexdmffile(filename):
    # read mesh from .msh file
    msh = meshio.read(filename+".msh")
    # write the gmsh file in xdmf format
    triangle_mesh = meshio.Mesh(points=msh.points,
                               cells={"triangle": msh.cells["triangle"]},
                               cell_data={"triangle": {"subdomains": 
                                  msh.cell_data["triangle"]["gmsh:physical"]}})
    meshio.write(filename+"_mf.xdmf", triangle_mesh)

    ## TODO (new version of meshio doesn't work)
    #for cell in msh.cells:
    #  if cell.type == "triangle":
    #    triangle_cells = cell.data
    #  elif  cell.type == "tetra":
    #    tetra_cells = cell.data
    #for key in msh.cell_data_dict["gmsh:physical"].keys():
    #    if key == "triangle":
    #        triangle_data = msh.cell_data_dict["gmsh:physical"][key]
    #        print(triangle_data)
    #triangle_mesh = meshio.Mesh(points=msh.points,
    #                            cells={"triangle": triangle_cells},
    #                            cell_data={"subdomains": [triangle_data]})

if __name__== "__main__":
  writexdmffile("compression_mesh_rounded_refine")
