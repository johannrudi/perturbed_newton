'''
=======================================
TODO

Author:             Johann Rudi
=======================================
'''

import sys
import fenics as fe

sys.path.append("..")
import Abstract.Bits

#======================================
# Boundary Conditions
#======================================

def _boundaryAll(x, on_boundary):    return on_boundary

def _boundaryTop(x, on_boundary):    return fe.near(x[1], 1.0)
def _boundaryRight(x, on_boundary):  return fe.near(x[0], 1.0)
def _boundaryBottom(x, on_boundary): return fe.near(x[1], 0.0)
def _boundaryLeft(x, on_boundary):   return fe.near(x[0], 0.0)

def _boundaryTopBottom(x, on_boundary):
    return _boundaryTop(x, on_boundary) or _boundaryBottom(x, on_boundary)
def _boundaryLeftRight(x, on_boundary):
    return _boundaryLeft(x, on_boundary) or _boundaryRight(x, on_boundary)

def createBoundaryConditions(boundary_condition_type, FncSp):
    '''
    Creates boundary conditions. Type can be one of:
        no-slip,
        no-outflow / free-slip,
        inflow
    '''
    print("Create boundary conditions: type=%s" % boundary_condition_type)
    if 'no-slip' == boundary_condition_type:
        FncSpVel = FncSp.sub(0)

        # create no-slip velocity Dirichlet BC's (all velocity comp. = 0)
        noslip = fe.Constant((0.0, 0.0))
        bc = fe.DirichletBC(FncSpVel, noslip, _boundaryAll)

    elif 'no-outflow' == boundary_condition_type or \
         'free-slip' == boundary_condition_type:
        FncSpVel = FncSp.sub(0)
        FncSpVelX = FncSpVel.sub(0)
        FncSpVelY = FncSpVel.sub(1)

        # create no-outflow velocity Dirichlet BC's (normal velocity comp. = 0)
        bc_tb = fe.DirichletBC(FncSpVelY, 0.0, _boundaryTopBottom)
        bc_lr = fe.DirichletBC(FncSpVelX, 0.0, _boundaryLeftRight)
        bc = [bc_tb, bc_lr]

    elif 'inflow' == boundary_condition_type:
        FncSpVel = FncSp.sub(0)

        # set no-slip boundary condition for velocity
        noslip = fe.Constant((0.0, 0.0))
        bc_tb = fe.DirichletBC(FncSpVel, noslip, _boundaryTopBottom)
   
        # set inflow boundary condition for velocity
        inflow = fe.Expression(('sin(x[1]*pi)', '0.0'))
        bc_l = fe.DirichletBC(FncSpVel, inflow, _boundaryLeft)
   
        # set boundary condition for pressure at outflow
        zero = fe.Constant(.00)
        bc_r = fe.DirichletBC(FncSpPress, zero, _boundaryRight)
   
        # collect boundary conditions
        bc = [bc_tb, bc_l, bc_r]

    else:
        raise UserWarning('Unknown type of boundary condition "%s"' % boundary_condition_type)
        bc = None

    return bc

def applyBoundaryConditions(u, boundary_condition, boundary_condition_type=None):
    assert boundary_condition is not None or boundary_condition_type is not None

    # apply given boundary conditions
    if boundary_condition is not None:
        if isinstance(boundary_condition, fe.DirichletBC):
            boundary_condition.apply(u.vector())
        else:
            for bc in boundary_condition:
                bc.apply(u.vector())
        return

    # create and apply boundary conditions
    if boundary_condition_type is not None:
        bc = createBoundaryConditions(boundary_condition_type, u.function_space())
        applyBoundaryConditions(u, bc)

#======================================
# Sinker and Plume Anomalies
#======================================

sphereRand64CenterX = [
  0.88438893850986044, 0.14846502294340846, 0.44565621143957523,
  0.30385162277765243, 0.79848582952154912, 0.23687978322605474,
  0.97370490267498400, 0.86009887685416064, 0.98523663941483031,
  0.72034320600074808, 0.88763686588960411, 0.99217530249530528,
  0.90134812303451162, 0.10843642785551477, 0.56714436189799555,
  0.66251609843508175, 0.96199376691617278, 0.69631446341112724,
  0.89003623322821313, 0.11394864199518917, 0.65199716721700318,
  0.28182023756086683, 0.75591412090796117, 0.11393063867842590,
  0.05064649767054341, 0.63020512499974968, 0.60315632043989653,
  0.03542346068278846, 0.10804603661555889, 0.55114033580441879,
  0.87223550392159788, 0.45964201011136652, 0.45187460904250087,
  0.74090530289358758, 0.42719355110128809, 0.36835084162880050,
  0.82905590877880064, 0.65050763538006051, 0.87757393430765984,
  0.17991487850424015, 0.58109322757849147, 0.86462201253355300,
  0.52892246632842854, 0.54327994624985110, 0.44454216287347137,
  0.62945048609947929, 0.34530786221516552, 0.95381302519792688,
  0.77502781398196741, 0.29553419616487600, 0.78485459109314959,
  0.32102321708895110, 0.57068285048187828, 0.69913355930120900,
  0.44621561169122159, 0.67537531841659471, 0.74719694413071300,
  0.13183066547551237, 0.14573209949629140, 0.82232622218530615,
  0.49263859846178115, 0.53852557446902116, 0.03642551552548012,
  0.36503262530574743
]
sphereRand64CenterY = [
  0.43898966680559026, 0.61981592726087100, 0.84399951147493613,
  0.48329456773428559, 0.98748757518935293, 0.70223663266789038,
  0.97230555568845700, 0.40188339800808559, 0.55947740598727547,
  0.48403851681234178, 0.19873675009892122, 0.40235161693594812,
  0.99538177651788629, 0.03611403079535602, 0.96196465780532600,
  0.52331331841200956, 0.54020403751352009, 0.51971615792087811,
  0.33020224251402053, 0.31092271345360578, 0.06616012459832676,
  0.88006625972605623, 0.60329637581534845, 0.97856388515997794,
  0.46620183379063596, 0.23029916377767379, 0.59987909583285700,
  0.51381483503827652, 0.45987562894231393, 0.80540431700799764,
  0.05219215242890096, 0.95853359612900957, 0.33342818646086647,
  0.50679452405550252, 0.16869027871227116, 0.94181779523988429,
  0.62659097495527594, 0.72662953230651417, 0.01436214394598578,
  0.92629426819795000, 0.63715122252522471, 0.05595252940702089,
  0.69435056062244160, 0.70252027215346380, 0.08539783081264496,
  0.79617906649845815, 0.94681666716159452, 0.07359563580819994,
  0.91418782124377351, 0.15184572231448457, 0.27083150215131735,
  0.82956180439916749, 0.57182963622884753, 0.79625794328157706,
  0.46566240836511064, 0.90366452636353989, 0.26051150708354609,
  0.12350083231980058, 0.58504361526871551, 0.72290297326846520,
  0.65488289844966852, 0.28220516455822231, 0.32624457306031240,
  0.30914961889591730
]

def _indicator_code(center=(0.5, 0.5), decay=200.0, width=0.1):
    '''
    Creates a string of CPP-code for an indicator with one sphere:

        Indc(x) = exp ( -decay * max (0, ||center - x|| - width/2)^2 )

    where
        x      --- coordinates (x,y)
        center --- coordinates of sphere center
        decay  --- exponential decay
        width  --- width of sphere with max indicator = 1
    '''
    val = "sqrt(pow(%e - x[0], 2) + pow(%e - x[1], 2))" % (center[0], center[1])
    val = "max(0.0, (%s) - %e/2.0)" % (val, width)
    val = "exp(-%e * pow(%s, 2))" % (decay, val)
    return "max(0.0, min(%s, 1.0))" % (val)

def _indicatorRandom_code(n_spheres=1, decay=200.0, width=0.1):
    '''
    Creates a string of CPP-code for an indicator with multiple randomly placed
    spheres.
    '''
    # return zero if nothing to do
    if 0 == n_spheres:
        return '0.0'

    # combine spheres
    indicator = []
    for i in range(0, n_spheres):
        center = (sphereRand64CenterX[i], sphereRand64CenterY[i])
        indicator.append('(' + _indicator_code(center, decay, width) + ')')
    return '+'.join(indicator)

def _sinkerRandom_code(n_spheres=1, decay=200.0, width=0.1, scaling=1.0):
    '''
    Creates a string of CPP-code for an anomaly of randomly placed (cold) sinkers,
    where one sphere is shaped like a Gaussian with values in range [0,1];

        dT(x) = c * ( 1.0 - exp(-decay * max(0, ||center - x|| - width/2)^2) )

    where
        x      --- coordinates (x,y)
        center --- coordinates of sinker center
        decay  --- exponential decay
        width  --- width of min temperature around the center
        c      --- scaling factor (typically 1)
    '''
    val = _indicatorRandom_code(n_spheres, decay, width)
    val = "%e * (1.0 - (%s))" % (scaling, val)
    return "max(0.0, min((%s), 1.0))" % (val)

def _plumeRandom_code(n_spheres=1, decay=200.0, width=0.1, scaling=1.0):
    '''
    Creates a string of CPP-code for an anomaly of randomly placed (hot) plumes,
    where one sphere is shaped like a Gaussian with values in range [0,1];

        dT(x) = c * exp(-decay * max(0, ||center - x|| - width/2)^2)

    where
        x      --- coordinates (x,y)
        center --- coordinates of sinker center
        decay  --- exponential decay
        width  --- width of min temperature around the center
        c      --- scaling factor (typically 1)
    '''
    val = _indicatorRandom_code(n_spheres, decay, width)
    val = "%e * (%s)" % (scaling, val)
    return "max(0.0, min((%s), 1.0))" % (val)

def temperatureAnomaly_code(n_sinkers=0, sinker_decay=200.0, sinker_width=0.1, 
                            n_plumes=0, plume_decay=200.0, plume_width=0.1):
    '''
    Creates a string of CPP-code for an anomaly with sinkers and plumes.
    '''
    if 0 < n_sinkers:
        print("  Temperature anomaly: sinkers=%i, decay=%g, width=%g" % 
              (n_sinkers, sinker_decay, sinker_width))
        val_sinker = _sinkerRandom_code(n_sinkers, sinker_decay, sinker_width, 1.0)
    else:
        val_sinker = '1.0'
    if 0 < n_plumes:
        print("  Temperature anomaly: plumes=%i, decay=%g, width=%g" % 
              (n_plumes, plume_decay, plume_width))
        val_plume = _plumeRandom_code(n_plumes, plume_decay, plume_width, 1.0)
    else:
        val_plume = '1.0'
    return "(%s)*(%s)" % (val_sinker, val_plume)

#======================================
# Viscosity
#======================================

def _viscosityAnomaly_code(anomaly_code, scaling, visc_min):
    '''
    Creates a string of CPP-code for viscosity with anomalies:
        visc(x) = (scaling - visc_min) * (1 - anomaly(x)) + visc_min
    '''
    assert 0.0 < visc_min
    return '(%e - %e) * fabs(1.0 - (%s)) + %e' % (scaling, visc_min, anomaly_code, visc_min)

def createViscosity(viscosity_type, scaling=1.0, visc_min=1.0e-2, visc_max=1.0e2, degree=1,
                    n_sinkers=0, sinker_decay=200.0, sinker_width=0.1, 
                    n_plumes=0, plume_decay=200.0, plume_width=0.1):
    '''
    Creates viscosity. Type can be one of:
        const,
        anomaly
    '''
    print("Create viscosity: type=%s, scaling=%g, min=%.1e, max=%.1e, degree=%i" % 
          (viscosity_type, scaling, visc_min, visc_max, degree))
    if 'const' == viscosity_type:
        visc = fe.Constant(scaling)
    elif 'anomaly' == viscosity_type:
        temp_code = temperatureAnomaly_code(n_sinkers, sinker_decay, sinker_width, 
                                            n_plumes, plume_decay, plume_width)
        visc_code = _viscosityAnomaly_code(temp_code, scaling, visc_min)
        visc = Abstract.Bits.enforceBounds(fe.Expression(visc_code, degree=degree), 
                                           vmin=visc_min, vmax=visc_max)
    else:
        raise UserWarning('Unknown type of viscosity "%s"' % viscosity_type)
        visc = None
    return visc

#======================================
# Right-Hand Side
#======================================

def _rhsAnomaly_code(anomaly_code='0.0', scaling=1.0, n_sinkers=0, n_plumes=0):
    """
    Creates a string of CPP-code for the right-hand side:
        rhs_x(x) = 0.0
        rhs_y(x) = scaling * (anomaly(x) - 0.5)
    """
    if 0 < n_sinkers and n_plumes <= 0:
        neutral_val = 1.0
    elif n_sinkers <= 0 and 0 < n_plumes:
        neutral_val = 0.0
    else:
        neutral_val = 0.5
    rhs_x = "0.0"
    rhs_y = "%e*((%s) - %e)" % (scaling, anomaly_code, neutral_val)
    return (rhs_x, rhs_y)

def createRightHandSide(rhs_type, scaling=1.0, degree=1,
                        n_sinkers=0, sinker_decay=200.0, sinker_width=0.1, 
                        n_plumes=0, plume_decay=200.0, plume_width=0.1,
                        scalingvec=[0.0,0.0]):
    '''
    Creates the right-hand side of the momentum equation. Type can be one of:
        zero,
        const,
        anomaly
    '''
    print("Create right-hand side: type=%s, scaling=%g, degree=%i" % (rhs_type, scaling, degree))
    if 'zero' == rhs_type:
        rhs = fe.Constant((0.0, 0.0))
    elif 'const' == rhs_type:
        rhs = fe.Constant((scaling, scaling))
    elif 'constvec' == rhs_type:
        rhs = fe.Constant((scalingvec[0], scalingvec[1]))
    elif 'anomaly' == rhs_type:
        temp_code = temperatureAnomaly_code(n_sinkers, sinker_decay, sinker_width, 
                                       n_plumes, plume_decay, plume_width)
        rhs_code  = _rhsAnomaly_code(temp_code, scaling, n_sinkers, n_plumes)
        rhs = fe.Expression(rhs_code, degree=degree)
    else:
        raise UserWarning('Unknown type of right-hand side "%s"' % rhs_type)
        rhs = None
    return rhs
