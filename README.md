## Requirements
|Name|Version|
|:----|:----|
|FEniCS|2019.1.0|
|SciPy|1.4.1|
|Matplotlib| 3.2.0|
|Mshr|2019.1.0|


Below are packages needed for generating the mesh for example1:

|Name|Version|
|:----|:----|
|Meshio|3.3.0|
|Gmsh|4.5.3|

## Usage
To create a python virtual environment with all requirements:
  `conda create -n fenics -c conda-forge fenics matplotlib scipy mshr gmsh meshio=3.3.0`

To run `./Stokes_vonMises/example1`:

* Activate python virtual environment `conda activate fenics`
* Generate the mesh via `sh meshgen_script.sh`
* Run the driver `python run_example1.py`

To run `./Stokes_vonMises/example2`:

* Activate python virtual environment `conda activate fenics`
* Run the driver `python run_example2.py`

## Notes
* Implementation of using finite element pair ![fempair](https://latex.codecogs.com/gif.latex?P_k-P_{k-1}^{\text{disc}}) is available for Example 1 (not yet available for Example 2). To use it, in addition to set the `DISCR` option in `run_example1.py`, one also needs to uncomment line 4 of the mesh generation script `meshgen_script.sh` before running `sh meshgen_script.sh`.

